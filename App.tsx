import React from "react";
import { StatusBar } from "expo-status-bar";
import { SafeAreaProvider } from "react-native-safe-area-context";
import * as Sentry from "sentry-expo";
import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import Navigation from "./navigation";
import { BottomSheetModalProvider } from "@gorhom/bottom-sheet";

Sentry.init({
  dsn: "https://public@sentry.example.com/1",
});

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <BottomSheetModalProvider>
          <Navigation colorScheme={colorScheme} />
          <StatusBar />
        </BottomSheetModalProvider>
      </SafeAreaProvider>
    );
  }
}
